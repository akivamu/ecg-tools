import matplotlib.pyplot as plt
import numpy as np

from .ishne_holter import Holter

def down_sample(array, step):
    data = array[::step]
    print('Down sample, step={}, from {} -> {} samples'.format(step, len(array), len(data)))
    return data


def plot_lead(lead):
    print('Lead: spec={}, qual={}'.format(lead.spec_str(), lead.qual_str()))
    lead_data = lead.data
    nsamples = len(lead_data)
    sr = x.sr
    duration_secs = nsamples / sr
    print('Lead has {} samples, sampling rate: {}Hz, duration: {}s'.format(len(lead_data), sr, duration_secs))

    # Down sample
    down_scale = 10
    sr = int(sr / down_scale)
    data = down_sample(lead_data, down_scale)

    x_axis = np.arange(0, duration_secs, 1/sr)

    plt.plot(x_axis, data)
    plt.ylabel('mV')
    plt.xlabel('Time')
    plt.show()


# Load header
print('Load header...')
x = Holter('D:\\temp\\sample_holter_01.ishne')

# Load data
# x.load_data()
x.load_data(1000, 5)

# Plot lead
plot_lead(x.lead[1])