class Lead:
    def __init__(self, spec, qual, res):
        """Store a lead's parameters (name, quality, and amplitude resolution).  Data
        (samples) from the lead will be loaded separately.

        Keyword arguments:
        spec -- numeric code from Table 1 of ISHNE Holter spec
        qual -- numeric code from Table 2 of ISHNE Holter spec
        res -- this lead's resolution in nV
        """
        self.spec = spec
        self.qual = qual
        self.res  = res
        self.data = None

    def __str__(self):
        return self.spec_str()

    def save_data(self, data, convert=True):
        """Replace the data array for this lead with a new one, optionally converting
        from ISHNE format (int16 samples) to floats (units = mV).

        Keyword arguments:
        data -- 1d numpy array of samples for this lead
        convert -- whether sample values should be converted to mV
        """
        if convert:
            data = data.astype(float)
            data *= self.res/1e6
        self.data = data

    def data_int16(self, convert=True):
        """Returns data in the format for saving to disk.  Pointless to use if convert==False."""
        data = self.data
        if convert:
            data *= 1e6/self.res
            data = data.astype(np.int16)
        return data
        # TODO?: maybe do this the other way around, save data unaltered as
        # int16 and make converted available as e.g. self.data_mV().  That may
        # reduce possibility of rounding errors during conversions.

    def spec_str(self):
        """Return this lead's human-readable name (e.g. 'V1')."""
        lead_specs = {
            -9: 'absent', 0: 'unknown', 1: 'generic',
            2: 'X',    3: 'Y',    4: 'Z',
            5: 'I',    6: 'II',   7: 'III',
            8: 'aVR',  9: 'aVL', 10: 'aVF',
            11: 'V1', 12: 'V2',  13: 'V3',
            14: 'V4', 15: 'V5',  16: 'V6',
            17: 'ES', 18: 'AS',  19: 'AI'
        }
        return lead_specs[self.spec]

    def qual_str(self):
        """Return a description of this lead's quality (e.g. 'intermittent noise')."""
        lead_quals = {
            -9: 'absent',
            0: 'unknown',
            1: 'good',
            2: 'intermittent noise',
            3: 'frequent noise',
            4: 'intermittent disconnect',
            5: 'frequent disconnect'
        }
        return lead_quals[self.qual]